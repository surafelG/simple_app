//this file is where the Database and its tables are created and tested
// the name of the database will be hotel
// and it will have two tables 1. users 2.guests
//1.users is where employee data is stored and used for authentication purposes.
// but for now it only holds one manager
//2.guests is the main table that will be used to save guest data.


const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('hotel.db');

// run each database statement step by step

db.serialize(() => { 
  

//this creates a table for the guests.
    db.run("CREATE TABLE IF NOT EXISTS guest (timeOfRecord TEXT, fullName TEXT NOT NULL, city TEXT, subCity TEXT, woreda TEXT, houseNumber TEXT, phoneNumber INTEGER NOT NULL, IDPassNumber TEXT NOT NULL, Purposeofstay TEXT, PRIMARY KEY (timeOfRecord, fullname)) ");
    //db.run("CREATE TABLE guest (timeOfRecord TEXT, fullName TEXT NOT NULL, city TEXT, subCity TEXT, woreda TEXT, houseNumber TEXT, phoneNumber INTEGER NOT NULL, IDPassNumber TEXT NOT NULL, Purposeofstay TEXT, PRIMARY KEY (timeOfRecord, fullname)) ");

 console.log('creating...');


//this is used for debugging purposes
// to check the insert and select functions

/*
db.run(`INSERT INTO guest VALUES ( datetime('now'), 'fasdf', 'f', 'f','f','1',222,'f','f')`);
 */
    db.each("SELECT * from guest", (err,row)=>{
        console.log(row.timeOfRecord+" "+row.fullName )
    });
  

});