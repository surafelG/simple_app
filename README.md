This a simple web application that allows a manager of a Guesthouse to store data about the guests.
And also be able to retrive the data. 

It uses sqlite3 database, Nodejs/Express back-end and React frontend.

Installation

After cloning the project. 

1. open the back directory on terminal/cmd i.e cd into 'simple-app/back'
            `cd simple_app/back`

2. then run `npm install`  this will allow the dependencies listed in the package.json file to be installed.

3. then run `node createDB.js` this will run the createDB.js file which creates the database and its tables.

4. then run `nodemon index.js`  --- to start the backend

5. then do the same for front directory

*  `cd simple_app/front` to go into the front directory
        
*  `npm install` to add the dependecies
        
*  and finally `npm start` to start the client side.
            
         
 Happy exploring!!!!!!!!!!!!!!!!!!!!!
    

